#!/usr/bin/env python3
"""
 read data from gamepad

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""
import asyncio
from concurrent.futures import ThreadPoolExecutor

from inputs import get_gamepad


def read_gamepad():
    while True:
        events = get_gamepad()
        for event in events:
            if event.ev_type == "Sync":
                continue
            print(event.ev_type, event.code, event.state)


async def main():
    loop = asyncio.get_running_loop()

    # Run the blocking function in a separate thread
    with ThreadPoolExecutor() as pool:
        await loop.run_in_executor(pool, read_gamepad)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
