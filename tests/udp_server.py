#!/usr/bin/env python3
"""
 Test server to receive data from gamepad

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""
import socket
import json


class UDP_Server:
    """UDP server to receive and print data"""

    def __init__(self, host: str = "127.0.0.1", port: int = 32095):
        self._host = host
        self._port = port
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.bind((self._host, self._port))
        print(f"UDP server listening on {host}:{port}")

    def listen(self):
        """Listen for incoming data and print it"""
        try:
            while True:
                data, addr = self._sock.recvfrom(1024)  # buffer size is 1024 bytes
                print(f"Received message from {addr}: {data.decode()}")
        except KeyboardInterrupt:
            print("Server stopped.")
        finally:
            self._sock.close()


if __name__ == "__main__":
    server = UDP_Server()
    server.listen()
