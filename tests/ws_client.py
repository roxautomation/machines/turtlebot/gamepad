#!/usr/bin/env python3
"""
 simulate sending of data to websocket server

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import asyncio
import websockets

URI = "ws://localhost:32090"


async def send_data():
    async with websockets.connect(URI) as websocket:
        idx = 0
        while True:
            await websocket.send(f"Client says: {idx}")
            await asyncio.sleep(1)
            idx += 1


if __name__ == "__main__":
    try:
        asyncio.run(send_data())
    except KeyboardInterrupt:
        print("stopped")
