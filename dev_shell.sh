#!/bin/bash

IMG_NAME="local/gamepad:dev"
WS_PORT=32090

# build docker image
docker build -t $IMG_NAME -f ./docker/Dockerfile.sys  ./docker

docker run --rm -it --privileged \
            --network=host \
            -v $(pwd):/workspaces/gamepad \
            -v /dev/input:/dev/input \
            -v /dev/bus/usb:/dev/bus/usb \
            $IMG_NAME /bin/bash
