# Gamepad to Websocket Microservice

## Overview

This repository contains a microservice designed to read input from a gamepad and send the data to mqtt broker.

## Build and run locally

`build_and_run.sh`


## Configuration

You can configure the microservice by setting the following environment variables:

- `MQTT_HOST`: mqtt broker, defaults to localhost
- `MQTT_PORT`: defaults to 1883
- `JS_TOPIC` : publish axes, defaults to `/gamepad/js`
- `BTN_TOPIC`: buttons topic, defaults to `/gamepad/btn`
- `LOG_LEVEL`: The level of logging desired (e.g., debug, info).

## Contributing

Contributions to the project are welcome. Please follow the standard fork-and-pull request workflow.


## Development


1. develop and test in devcontainer (VSCode)
2. build `app` image by bumping version with a tag.


## What goes where
* `gamepad` app code. It is *not* a python package. Needs to be added to `PYTHONPATH`.
* `docker` folder contains dockerfiles for building base system and app images.
* `.gitlab-ci.yml` takes care of the building steps.




## License

This project is licensed under [MIT License](LICENSE). Please see the `LICENSE` file for more details.

