#!/bin/bash

IMG_NAME="rox-gamepad"

# copy source code
rm -rf docker/src
cp -r gamepad docker/src

# build docker image
docker build -t $IMG_NAME -f ./docker/Dockerfile.app  ./docker

# set debug level
if [ -z "${LOG_LEVEL}" ]; then
    LOG_LEVEL='info'
fi


# run docker image
docker run --rm -it --privileged \
            -v $(pwd):/workspace \
            -v /dev/input:/dev/input \
            -v /dev/bus/usb:/dev/bus/usb \
            --network=host \
            -e LOG_LEVEL=${LOG_LEVEL} \
            $IMG_NAME
