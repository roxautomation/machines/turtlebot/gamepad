#!/usr/bin/env python3
"""
  Main entry point for the application.
"""
import asyncio
import logging
import os
from concurrent.futures import ThreadPoolExecutor

import aiomqtt
import inputs  # type: ignore
import orjson

from gamepad import __version__

# get mqtt settings from environment variables
HOST = os.environ.get("MQTT_HOST", "127.0.0.1")
PORT = int(os.environ.get(" MQTT_PORT", 1883))
JS_TOPIC = os.environ.get("MQTT_TOPIC", "/gamepad/js")
BTN_TOPIC = os.environ.get("MQTT_TOPIC", "/gamepad/btn")

MAX_INT = 32767
CENTER_THRESHOLD = 3


# button mappings
BTN_MAP = {
    "BTN_SOUTH": "A",
    "BTN_EAST": "B",
    "BTN_NORTH": "X",
    "BTN_WEST": "Y",
    "BTN_START": "START",
    "BTN_SELECT": "BACK",
}

# define axis mapping
JOY0 = ("ABS_Y", "ABS_X")
JOY1 = ("ABS_RY", "ABS_RX")

SCALE = 100 / MAX_INT  # scale to -100 to 100


# get logging level from environment variable
level_var = os.environ.get("LOG_LEVEL", "INFO")
if level_var.lower() == "debug":
    log_level = logging.DEBUG
else:
    log_level = logging.INFO


logging.basicConfig(
    level=log_level,
    format="%(asctime)s.%(msecs)03d %(levelname)s: %(message)s",
    datefmt="%H:%M:%S",
)


def print_info():
    """print version and topics"""
    print(f"gamepad version: {__version__}")
    print(f"mqtt broker: {HOST}:{PORT}")

    print(f"mqtt topics: {JS_TOPIC} , {BTN_TOPIC}")


class GamepadReader:
    """gamepad interface. Reads joystick and publishes json to udp"""

    def __init__(self) -> None:
        self.gamepad = inputs.devices.gamepads[0]
        # Vibrate
        # self.gamepad.set_vibration(1, 0, 500)

        # two joysticks
        self.joysticks = [[0, 0], [0, 0]]

        # active when not in zero position. Send zeros when going from active to inactive
        self.active = False

        self.msg_queue: asyncio.Queue[str] = asyncio.Queue()
        self.btn_queue: asyncio.Queue[str] = asyncio.Queue()

    def read_gamepad(self):
        while True:
            events = inputs.get_gamepad()

            for event in events:
                if event.ev_type == "Sync":
                    continue

                # logging.debug(f"{event.code=} {event.state=}")

                # check joystick 1
                if event.code in JOY0:
                    idx = JOY0.index(event.code)
                    self.joysticks[0][idx] = -int(event.state * SCALE)

                # check joystick 2
                if event.code in JOY1:
                    idx = JOY1.index(event.code)
                    self.joysticks[1][idx] = -int(event.state * SCALE)

                # check buttons
                if event.code.startswith("BTN_"):
                    if event.code in BTN_MAP:
                        event.code = BTN_MAP[event.code]
                    self.btn_queue.put_nowait(
                        {"button": event.code, "state": event.state}
                    )

            # crash if gamepad is disconnected

    def zero_positioin(self) -> bool:
        """check if both joysticks are in zero position"""
        for js in self.joysticks:
            x, y = js
            if abs(x) > CENTER_THRESHOLD or abs(y) > CENTER_THRESHOLD:
                return False
        return True

    def json_str(self) -> str:
        """return json string"""

        data = {"js0": self.joysticks[0], "js1": self.joysticks[1]}
        json_str = orjson.dumps(data).decode("utf-8")  # pylint: disable=no-member

        logging.debug(f"json_str: {json_str}")
        return json_str

    async def send_loop(self):
        """send data to websocket server"""
        while True:
            try:
                logging.info(f"connecting to {HOST}:{PORT}")
                async with aiomqtt.Client(HOST, PORT) as client:
                    while True:
                        if not self.zero_positioin():
                            await client.publish(JS_TOPIC, self.json_str())
                            if not self.active:
                                logging.info("activating")
                                self.active = True
                        else:
                            if self.active:
                                # send zeros
                                logging.info("deactivating")
                                await client.publish(JS_TOPIC, self.json_str())
                                self.active = False

                        # check for button events
                        try:
                            btn_event = self.btn_queue.get_nowait()
                            logging.debug(f"button event: {btn_event}")
                            await client.publish(BTN_TOPIC, orjson.dumps(btn_event))
                        except asyncio.QueueEmpty:
                            pass

                        await asyncio.sleep(
                            0.1
                        )  # Sleep for 0.1 seconds (10 times per second)

            except ConnectionRefusedError:
                logging.info("connection refused, retrying in 5 seconds")
                await asyncio.sleep(5)

            except Exception as err:  # pylint: disable=broad-except
                logging.error(f"error: {err}")
                await asyncio.sleep(1)


async def main():
    print_info()
    reader = GamepadReader()

    loop = asyncio.get_running_loop()

    # Run the blocking function in a separate thread
    with ThreadPoolExecutor() as pool:
        gamepad_task = loop.run_in_executor(pool, reader.read_gamepad)
        send_loop_task = asyncio.create_task(reader.send_loop())

        # Wait for either of the tasks to complete
        done, _ = await asyncio.wait(
            [gamepad_task, send_loop_task], return_when=asyncio.FIRST_COMPLETED
        )

        # Check if the send_loop_task is done and has an exception
        if send_loop_task in done:
            exception = send_loop_task.exception()
            if exception:
                logging.exception(
                    "Unhandled exception in send_loop", exc_info=exception
                )
        os._exit(1)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Program interrupted by user.")
    except Exception as e:  # pylint: disable=broad-except
        print(f"Program stopped due to an unhandled exception: {e}")
